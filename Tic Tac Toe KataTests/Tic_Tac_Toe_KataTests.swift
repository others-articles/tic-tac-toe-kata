//
//  Tic_Tac_Toe_KataTests.swift
//  Tic Tac Toe KataTests
//
//  Created by Matt Jacquet on 9/10/20.
//  Copyright © 2020 Matt Jacquet - Appro. All rights reserved.
//

import XCTest
@testable import Tic_Tac_Toe_Kata

class Tic_Tac_Toe_KataTests: XCTestCase {
    
    
    func testPlayerXGoesFirst() {
        let game = GameViewModel()
        XCTAssertEqual("cross.png", game.getPlayerImageName())
    }
    
    func testCannotPlayOnPlayedPosition() {
        let game = GameViewModel();
        game.positions = [1: .player2, 2: .empty, 3: .player2, 4: .empty, 5: .player1, 6: .empty, 7: .empty, 8: .empty, 9: .empty]
        XCTAssertEqual(false, game.canSelectPosition(tag: 1));
    }

    func testCannotPlayWhileAllPositionsAreSelected() {
        let game = GameViewModel();
        game.positions = [1: .player2, 2: .player1, 3: .player2, 4: .player1, 5: .player1, 6: .player2, 7: .player2, 8: .player1, 9: .player1]
        let values = game.positions.values
        if !values.contains(.empty){
            XCTAssertEqual(false, game.canSelectPosition(tag: 9))
        }
    }
    
    func testCheckPlayerHasWinningCombination(){
        
        let game = GameViewModel();
        game.playedPositionsPlayerOne = [1,3,5,7]
        let result = game.checkPlayerHaveWinningCombination()
        XCTAssertEqual(true, result)
        
        game.playedPositionsPlayerOne = [1, 3, 7, 9]
        let result2 = game.checkPlayerHaveWinningCombination()
        XCTAssertEqual(false, result2)
        
        game.playedPositionsPlayerOne = [1,3,5,7]
        let result3 = game.checkPlayerHaveWinningCombination()
        XCTAssertEqual(true, result3)
        
        game.playedPositionsPlayerOne = [5,2,9]
        let result4 = game.checkPlayerHaveWinningCombination()
        XCTAssertEqual(false, result4)
    }
    
    func testGameIsDrawWhenAllPositionsSelectedWithNoWinner() {
        let game = GameViewModel();
        game.positions = [1: .player2, 2: .player1, 3: .player2, 4: .player1, 5: .player1, 6: .player2, 7: .player2, 8: .player1, 9: .player1]
        game.checkGameState()
        XCTAssertEqual(.draw, game.gameState)
    }
    
}
