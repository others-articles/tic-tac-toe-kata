//
//  PlayerState.swift
//  Tic Tac Toe Kata
//
//  Created by Matt Jacquet on 9/11/20.
//  Copyright © 2020 Matt Jacquet - Appro. All rights reserved.
//


enum PlayerState: String {
    case player1 = "Player one"
    case player2 = "Player two"
    case empty = "No Player"
}
