//
//  EndGameState.swift
//  Tic Tac Toe Kata
//
//  Created by Matt Jacquet on 9/11/20.
//  Copyright © 2020 Matt Jacquet - Appro. All rights reserved.
//

enum EndGameState {
    case won
    case draw
    case open
}
