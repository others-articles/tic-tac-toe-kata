//
//  Game.swift
//  Tic Tac Toe Kata
//
//  Created by Matt Jacquet on 9/10/20.
//  Copyright © 2020 Matt Jacquet - Appro. All rights reserved.
//

import UIKit

class GameViewModel {
    
    var gameState: EndGameState?
    var playerTurn: PlayerState = .player1
    var positions: [Int: PlayerState] = GameConst.START_STATE
    
    var playedPositionsPlayerOne: [Int] = []
    var playedPositionsPlayerTwo: [Int] = []
    
    func canSelectPosition(tag: Int) -> Bool {
        var result = false
        if positions[tag] == .empty {
            positions[tag] = playerTurn
            addPlayerPosition(tag: tag)
            result = true
        }else {
            result = false
        }
        return result
    }
    
    func resetGame() {
        playerTurn = .player1
        positions = GameConst.START_STATE
        playedPositionsPlayerOne = []
        playedPositionsPlayerTwo = []
    }
    
    
    func changePlayerTurn() {
        if playerTurn == .player1 {
            playerTurn = .player2
        }else if playerTurn == .player2 {
            playerTurn = .player1
        }
    }
    
    func checkGameState() {
        
        if checkPlayerHaveWinningCombination() {
            gameState = .won
            return 
        }
        
        if checkAllPositionsSelected() {
            gameState = .draw
        }else {
            gameState = .open
        }
    }
    
    func checkAllPositionsSelected() -> Bool{
        var result = true
        let values = positions.values
        if values.contains(.empty){
            result = false
        }

        return result
    }
    
    func addPlayerPosition(tag: Int) {
        if playerTurn == .player1 {
            playedPositionsPlayerOne.append(tag)
        }else {
            playedPositionsPlayerTwo.append(tag)
        }
    }
    
    func checkPlayerHaveWinningCombination() -> Bool {
        var result = false
        if playerTurn == .player1 {
            result = self.hasWinningCombination(playerPositions: playedPositionsPlayerOne)
        }else if playerTurn == .player2 {
            result = self.hasWinningCombination(playerPositions: playedPositionsPlayerTwo)
        }
        return result
    }
    
    func getPlayerImageName() -> String {
        var result = ""
        if playerTurn == .player1 {
            result = "cross.png"
        }else if playerTurn == .player2 {
            result = "circle.png"
        }
        return result
    }
    
    func hasWinningCombination(playerPositions: [Int])-> Bool {
        var result = false
        for winningCombinaison in GameConst.WINNING_COMBINATIONS {
            if playerPositions.contains(winningCombinaison[0]) && playerPositions.contains(winningCombinaison[1]) && playerPositions.contains(winningCombinaison[2]) {
                result = true
            }
        }
        return result
    }
}

struct GameConst {
    static let START_STATE: [Int: PlayerState] = [1: .empty, 2: .empty, 3: .empty, 4: .empty, 5: .empty, 6: .empty, 7: .empty, 8: .empty, 9: .empty]
    static let WINNING_COMBINATIONS = [[1, 5, 9], [3, 5, 7], [1, 4, 7], [2, 5, 8], [3, 6, 9], [1, 2, 3], [4, 5, 6], [7, 8, 9]]
}






