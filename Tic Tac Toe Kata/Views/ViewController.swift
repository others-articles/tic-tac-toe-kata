//
//  ViewController.swift
//  Tic Tac Toe Kata
//
//  Created by Matt Jacquet on 9/10/20.
//  Copyright © 2020 Matt Jacquet - Appro. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet var row1: UIButton!
    @IBOutlet var row2: UIButton!
    @IBOutlet var row3: UIButton!
    @IBOutlet var row4: UIButton!
    @IBOutlet var row5: UIButton!
    @IBOutlet var row6: UIButton!
    @IBOutlet var row7: UIButton!
    @IBOutlet var row8: UIButton!
    @IBOutlet var row9: UIButton!
    
    var game: GameViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        game = GameViewModel()
    }
    
    @IBAction func selectPosition(sender: UIButton) {
        if game.canSelectPosition(tag: sender.tag) {
            sender.setImage(UIImage(named: game.getPlayerImageName()), for: .normal)
            checkGameEvent()
            game.changePlayerTurn()
        }
    }
    
    func checkGameEvent() {
        game.checkGameState()
        switch game.gameState {
        case .draw:
            alert("", message: "The game is draw!")
            game.resetGame()
        case .won:
            alert("", message: "\(game.playerTurn.rawValue) has won the game!")
            game.resetGame()
        case .open:
            break
        default:
            break
        }
    }
    
    func resetGameView() {
        row1.setImage(nil, for: .normal)
        row2.setImage(nil, for: .normal)
        row3.setImage(nil, for: .normal)
        row4.setImage(nil, for: .normal)
        row5.setImage(nil, for: .normal)
        row6.setImage(nil, for: .normal)
        row7.setImage(nil, for: .normal)
        row8.setImage(nil, for: .normal)
        row9.setImage(nil, for: .normal)
    }
    
    func alert(_ title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: UIAlertAction.Style.cancel, handler: ({
            (_) in
            self.resetGameView()
        }))
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
}


